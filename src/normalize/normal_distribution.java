package normalize;

/* 
 * 2014-12-01
 * Author: Cholerae
 * Email: choleraehyq@gmail.com
 */

import java.util.*;

public class normal_distribution {
	
	private static double generate_average(ArrayList<Double> data) {
		double sum = 0;
		for (int i = 0; i < data.size(); i++) {
			sum += data.get(i);
		}
		return sum / data.size();
	}
	
	private static double generate_standard_diviation(ArrayList<Double> data, double avg) {
		double ret = 0;
		for (int i = 0; i < data.size(); i++) {
			ret += (data.get(i)-avg) * (data.get(i)-avg);
		}
		return ret;
	}
	
	public static double normalize(ArrayList<Double> data) {
		double y = 0.25;
		//Please adjust it after the experiment.
		double sum = 0, avg = generate_average(data), std_dva = generate_standard_diviation(data, avg);
		double n = 0;
		for (Double i : data) {
			if ( (i <= avg + y*std_dva) && (i >= avg - y*std_dva) ) {
				sum += i;
				n++;
			}
		}
		return sum/n;
	}
	
}