package clientNetworkAPI;

import java.io.IOException;
import java.util.ArrayList;

import clientNetworkAPI.Rssfingers;
import clientNetworkAPI.NetworkAPI;

//Maybe I should write a parent class and inherit it instead of C-V;
//Just write along. Maybe I will refactor it later :-)

//I refactor it a day later. 

public class MatchAPI extends NetworkAPI {
	
	public String sendTheData(ArrayList<Rssfingers> sendData) {
		String recvBuf = new String();;
		try {
			Rssfingers firstOfSend = new Rssfingers();
			firstOfSend.pointId = 0;// This number is different from the BuildDbAPI.
			toServer.writeObject(firstOfSend);
			
			for (Rssfingers send: sendData) {
				toServer.writeObject(send);
			}
			
			Rssfingers endOfSend = new Rssfingers();
			endOfSend.pointId = -2;
			toServer.writeObject(endOfSend);		
			
			//receive data from the server
			byte[] buffer = new byte[1024];
			int lenght = fromServer.read(buffer);
			//the data to be return
			recvBuf = new String(buffer,0,lenght);
		}
		catch (IOException ex) {
			ex.printStackTrace();
		}
		return recvBuf.trim();
	}
}
