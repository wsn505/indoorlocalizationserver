package clientNetworkAPI;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class NetworkAPI {
	protected Socket socket = null;
	//private static final int port = 8000;
	protected ObjectOutputStream toServer = null;
	protected InputStream fromServer =null;

	public void connectTheServer(String serverIP, int port) {
		try {
			socket = new Socket(serverIP, port);
			toServer = new ObjectOutputStream(socket.getOutputStream());
			fromServer = socket.getInputStream();
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public void closeTheConnection() {
		try {
			fromServer.close();
			toServer.close();
			socket.close();
		}
		catch (IOException ex) {
			ex.printStackTrace();
		}
	}
}
