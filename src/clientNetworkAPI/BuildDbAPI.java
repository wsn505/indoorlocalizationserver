package clientNetworkAPI;

/* 
 * 2014-12-01
 * Author: Cholerae 
 * Email: choleraehyq@gmail.com
 */

import java.io.*;
import java.util.*;

import clientNetworkAPI.Rssfingers;
import clientNetworkAPI.NetworkAPI;

public class BuildDbAPI extends NetworkAPI {

	public String sendTheData(ArrayList<Rssfingers> sendData) {
		String recvBuf = null;
		try {
			Rssfingers firstOfSend = new Rssfingers();
			firstOfSend.pointId = 1;//Declare that this is to build the db.
			toServer.writeObject(firstOfSend);   //OutPutStream:toServer
			
			for (Rssfingers send: sendData) {
				toServer.writeObject(send);
			}
			
			Rssfingers endOfSend = new Rssfingers();
			endOfSend.pointId = -2;
			toServer.writeObject(endOfSend);
			
			//receive data from the server
			byte[] buffer = new byte[1024];
			int lenght = fromServer.read(buffer);
			//the data to be return
			recvBuf = new String(buffer,0,lenght);
		}
		catch (IOException ex) {
			ex.printStackTrace();
		}
		return recvBuf.trim();
	}
	
}