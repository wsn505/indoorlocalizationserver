package match;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import normalize.normal_distribution;
import clientNetworkAPI.Rssfingers;

import common.DBHelper;
import common.Messages;

public class FingerMatch {
	
	//sql语句
	String sql =  new String("");
	//定义定位的策略，该策略可有多种，最后可对比效果
	private int locationCategory;
	
	public FingerMatch(int locationCategory){
		this.locationCategory = locationCategory;		
		Messages.ShowMesg("已选择定位策略:"+locationCategory);
	}
	
	// the true match process
	public String FingersMatch(HashMap<String, Double> hasMap_AddrRss,
				List<HashMap<String, Double>> list_hasMap_FingerInDB) {
			// TODO Auto-generated method stub
			HashMap<Integer,Double> hashID = new HashMap<Integer,Double>();
			int pointid = 1;
			//list_hasMap_fingerInDB is in order by pointid
			for(HashMap<String,Double> hashmap :list_hasMap_FingerInDB ){			
				 hashID.put(pointid, ComputingDistence(hasMap_AddrRss,hashmap));
				 pointid++;
			}
			List<Map.Entry<Integer,Double>> list_Data = new ArrayList<Map.Entry<Integer,Double>>(hashID.entrySet());
			
			//排序，为了得到若干个极有可能的点
			Collections.sort(list_Data, new Comparator<Map.Entry<Integer,Double>>()
			{  
				   public int compare(Map.Entry<Integer,Double> o1, Map.Entry<Integer,Double> o2)
				   {
				        if(o2.getValue()!=null && o1.getValue()!=null && o2.getValue().compareTo(o1.getValue())<0){
				      	   return 1;
				        }else{
				      	   return -1;
				        }}
			});		

			Messages.ShowMesg("已选择定位策略:"+locationCategory);
			//策略选择
		    if(locationCategory == 1)
		    {
				//策略1：最近的一个点
				int points =list_Data.get(0).getKey();
				String Location = GetTheLocationOfPoints(points);
				return Location; 
		    }
		    else if(locationCategory == 2)
		    {
		    	//策略2： 最近的三个点 ,利用三点定位的方法:根据圆心和半径做三个圆，求三个圆的交点
				Circle[] circleArray = new Circle[3];	
				//取得临近的points 目前取出3个points
				for(int indexPoint =0;indexPoint<3;indexPoint++){
					Map.Entry<Integer,Double> map = list_Data.get(indexPoint);
					String locationstr = GetTheLocationOfPoints(map.getKey());				
					circleArray[indexPoint] = new Circle(Double.parseDouble(locationstr.split(";")[0]),Double.parseDouble(locationstr.split(";")[1]),map.getValue());
				}
				return ComputingPointFromThreeCiecle(circleArray);
		    }
		    else{
		    	//第三种策略
		    	return "-1;-1";
		    }
		}//end the action
		
		/*
		 * 获取三个园的交点
		 */
		private String ComputingPointFromThreeCiecle(Circle[] circleArray){
			//获得第0个和第1个园的交点，可能有两个，也可能有1个
			CirIntersect cisect = new CirIntersect(circleArray[0],circleArray[1]);
			List<Double> list1 = cisect.intersect();  //存储第0个点和第1个点的交点
		    cisect = new CirIntersect(circleArray[2],circleArray[1]);
			List<Double> list2 = cisect.intersect();  //存储第2个点和第1个点的交点
			
			//获取到重复的坐标，即交点，即最终的结果
			for(int indexOfList1=0;indexOfList1<list1.size();indexOfList1=+2){
				double x = list1.get(indexOfList1);
				double y = list1.get(indexOfList1+1);
				for(int indexOfList2=0;indexOfList2<list2.size();indexOfList2=+2){
					if(x == list2.get(indexOfList2)&&y==list2.get(indexOfList2+1)){
						Messages.ShowMesg("The Final Point has been found!");
						return x+";"+y;
					}
				}
			}
			return "-1;-1";
		}
		
		/*
		 * 根据Points 编号获取位置 x,y
		 */
		private String GetTheLocationOfPoints(int points) {
			
			//查询字段尽量不要包括不必要的字段
			sql = "select x,y from points where pointid="+points;
			
			String location = new String();
			try {
				ResultSet rs_xy = DBHelper.executeQuery(sql);
				while(rs_xy.next()){
					//用户的位置
					 location+=rs_xy.getString("x")+";"+rs_xy.getString("y");
				}
				return location;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return "-1;-1";  //返回错误信息
		}

		/*
		 * 
		 * 
		 */
		private double ComputingDistence(HashMap<String, Double> hasMap_AddrRss,
				HashMap<String, Double> hasMap_FingerInDB){
				double distince = 0.0;
				List<String> list_mac = new ArrayList<String>();
				Iterator itot = hasMap_AddrRss.keySet().iterator();
				while(itot.hasNext()){
					Object o = itot.next();
					list_mac.add(o.toString());			
				}
				for(String str_mac:list_mac){
					distince += Math.pow(hasMap_AddrRss.get(str_mac)-hasMap_FingerInDB.get(str_mac),2);
				}
				return distince;
		}
		//get the HashMap of Addr and Rss from DataBase
		public List<HashMap<String, Double>> GetFingetPrintFromDataBase(){			
			List<HashMap<String, Double>> list_hashAddrRss = new ArrayList<HashMap<String, Double>>();
			sql = "select macaddr,rss from rss_fingers order by pointid;";

			try {
				   //query it in database
					ResultSet rs_fingers = DBHelper.executeQuery(sql);
					while(rs_fingers.next()){
						HashMap<String, Double> hashAddrRss = new HashMap<String,Double>();
						//required operations by split macaddr 
						String[] mac_ary = rs_fingers.getString("macaddr").split(";");
						String[] rss_ary = rs_fingers.getString("rss").split(";");
						for(int macrssIndex=0;macrssIndex<mac_ary.length && macrssIndex<rss_ary.length;macrssIndex++)
						{
							hashAddrRss.put(mac_ary[macrssIndex],Double.parseDouble(rss_ary[macrssIndex]));
						}	
						list_hashAddrRss.add(hashAddrRss);
					}
					
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			return list_hashAddrRss;
		}//end the action
		
		//normalize the rssfingers from client required location
		public HashMap<String, Double> GetNormalFingers(ArrayList<Rssfingers> a) {
			
			// TODO Declare some useful lists
			ArrayList<String> macaddr = new ArrayList<String>();   						 //store the mac addr
			HashMap<String, Double> macrss = new HashMap<String, Double>(); //store the hashmap of macaddr and rss
			
			//mac address scanning and put them into the valiarable macaddr 
			for(Rssfingers rssf : a){
				Set<String> set_str = rssf.addrRss.keySet();
				for(String str : set_str){
					macaddr.add(str.toString());
				}
			}//end the scanning action
			
			//Generate the HashMap of macaddr and rss
			for(String str_mac : macaddr){
				ArrayList<Double> arylist_rss = new ArrayList<Double>();
				for(Rssfingers rssf : a){
					ArrayList<Integer> list_int = rssf.addrRss.get(str_mac);
					for(Integer rss_int : list_int)
					{
						arylist_rss.add((double)rss_int);
					}			   
				}
				macrss.put(str_mac, normal_distribution.normalize(arylist_rss));
			}//end the action,result:"macrss generated"		
			return macrss;  //return the hashmap of mac and rss
		}  //end the action
}
