package common;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
 
public class DBHelper {
     private static Connection getCon() {
          Connection con = null;
          try {        	   
        	   String driver = "com.mysql.jdbc.Driver"; //数据库驱动
               String url = "jdbc:mysql://10.13.32.141:3306/rssfingers";
               String user = "root"; //用户名
               String password = "swpuhao123";//密码
               Class.forName(driver); //加载数据库驱动
               con = DriverManager.getConnection(url, user, password);
           } catch (Exception e) {
                e.printStackTrace();
           }
           return con;
      } 
      //查询语句
      public static ResultSet executeQuery(String sql) throws SQLException {
           Connection con = getCon();
           Statement stmt = con.createStatement();
           ResultSet rs = null;
           try
           {
        	   rs = stmt.executeQuery(sql);
               return rs;
           }catch(SQLException e){    	  
         	  	e.printStackTrace();
         	  	return null;
           }
           finally{        	  
        	
           }             
      }
      public static ResultSet executeQuery(String sql, Object... obj)   throws SQLException {
         Connection con = getCon();
         PreparedStatement pstmt = con.prepareStatement(sql);
          ResultSet rs = null;
         for (int i = 0; i < obj.length; i++) {
               pstmt.setObject(i + 1, obj[i]);
         }
         try
         {
        	  rs = pstmt.executeQuery();
              return rs;
         }catch(SQLException e){    	  
       	  	e.printStackTrace();
       	  	return null;
         }
         finally{   
//        	 if(pstmt!= null) 
//        		 pstmt.close(); 		
//        	 if(con!= null) 
//        	        con.close(); 
         }       
     }
     //执行增删改
     public static int executeNonQuery(String sql) throws SQLException {
         Connection con = getCon();
         Statement stmt = con.createStatement();
         try
         {
       	  	return stmt.executeUpdate(sql);
         }catch(SQLException e){    	  
       	  	e.printStackTrace();
       	  	return -1;
         }
         finally{
        	 if(stmt!= null) 
        		 stmt.close(); 		
        	 if(con!= null) 
        	        con.close(); 
         }
    }
    public static int executeNonQuery(String sql, Object... obj) throws SQLException {
      Connection con = getCon();
      PreparedStatement pstmt = con.prepareStatement(sql);
      for (int i = 0; i < obj.length; i++) {
             pstmt.setObject(i + 1, obj[i]);
      }
      try
      {
    	  return pstmt.executeUpdate();
      }catch(SQLException e){    	  
    	  e.printStackTrace();
    	  return -1;
      }
      finally{
    		 if(pstmt!= null) 
    			 pstmt.close(); 		
        	 if(con!= null) 
        	        con.close(); 
      }
    }
}
