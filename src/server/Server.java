package server;

/* 
 * 2014-12-01
 * Author: Cholerae
 * Email: choleraehyq@gmail.com
 */
import java.net.*;
import java.io.*;
import java.util.concurrent.*;

import server.DAO;
import server.HandleAClient;

public class Server {
    private static final int nThreads = 10;
    //number of threads in the threadpool;
    private static final int Port = 8888;
    //Listened port by server;
    private DataOutputStream cmdOutput = new DataOutputStream(System.out);
    //console output used to print some server status;
    private BufferedReader cmdInput = new BufferedReader(
            new InputStreamReader(System.in));
    //console input used to read some command to server 
    //enter "shutdown" to shutdown the server;
    private volatile boolean isShutdown = false;
    //used to judge whether the "shutdown" command is enter;
    private ExecutorService executor = Executors.newFixedThreadPool(nThreads);
    private ServerSocket ss = null;
 
    public DAO DBA = new DAO();
    
    public static void main(String[] args) {	
        new Server();
    }

    public Server() {
        try {
            ss = new ServerSocket(Port);

            cmdOutput.writeBytes("server started at "+ new java.util.Date() + '\n');

            int clientNo = 1;

            ListenCmd supervisor =new ListenCmd();
            new Thread(supervisor).start();
            
            DBA.LinkTheDB("Map");
            DBA.LinkTheDB("RSSDB");

            while (!isShutdown) {
                Socket client = ss.accept();

                cmdOutput.writeBytes("Connecting client "+ clientNo + '\n');
                InetAddress cliaddr = client.getInetAddress();
                cmdOutput.writeBytes("client" + clientNo + "'s IP is " + cliaddr.getHostAddress()+ '\n');
                //cmdOutput.writeBytes("client" + clientNo + "'s host is" + cliaddr.getHostName()+ '\n');

                HandleAClient task = new HandleAClient(client, DBA);
                executor.execute(task);

                clientNo++;
            }
            DBA.closeDB();
            cmdOutput.writeChars("Server will now shutdown...\n");
        }
        catch (Exception ex) {
            if (ex.getMessage().indexOf("Socket closed") != -1) 
                return;
            ex.printStackTrace();
        }
        finally {
            try{
                cmdOutput.close();
            }
            catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    class ListenCmd implements Runnable {
    	//To listen the cmdInput to wait for the "shutdown" command;
        public void run() {
            String cmd = new String();
            try{ 
                cmd = cmdInput.readLine();
                while (!cmd.equals("shutdown"))
                    cmd = cmdInput.readLine();
                shutdown();
            }
            catch (IOException ex) {
                ex.printStackTrace();
            }
        }

        private synchronized void shutdown() {
            isShutdown = true;
            executor.shutdown();
            try {
                cmdInput.close();
                ss.close();
            }
            catch (Exception ex) {
                System.err.println(ex);
            }
        }
    }

}
