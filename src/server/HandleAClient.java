package server;

/* 
 * 2014-12-04
 * Author: Cholerae
 * Email: choleraehyq@gmail.com
 */

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import match.FingerMatch;

import common.DBHelper;
import common.Messages;

import clientNetworkAPI.Rssfingers;
import server.DAO;

class HandleAClient implements Runnable {
	
	Socket client = null;
	ObjectInputStream input = null;
	OutputStream output = null;
	DAO DBA;
	FingerMatch fingermatch;;
	
	String str_sql;
	public HandleAClient(Socket socket, DAO dBA) {
		client = socket;
		DBA = dBA;
		try {
			output = client.getOutputStream();
			input = new ObjectInputStream(client.getInputStream());
		}
		catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public void run() {
		int fingerId = 0;
		try {
			//receive the object "rssrecord"
			Rssfingers recv = (Rssfingers)input.readObject();
			if (recv.pointId == 1) {  // check the class ClientNetworkAPI->BuildDbAPI  and you will found the reason
				//This is a BuildDbAPI process.
				ArrayList<Rssfingers> a = new ArrayList<Rssfingers>();
				recv = (Rssfingers)input.readObject();				
				Messages.ShowMesg("Begin the building process......");
				Iterator iter = recv.addrRss.entrySet().iterator();
				while (iter.hasNext()) {
					Map.Entry<String, ArrayList<Integer>> entry = (Map.Entry<String, ArrayList<Integer>>) iter.next();
					String key = entry.getKey();
					Messages.ShowMesg(key);
					ArrayList<Integer> val = entry.getValue();
					for (Integer i: val) {
						Messages.ShowMesg(i);
					}
				}				
				while (recv.pointId != -2) {
					a.add(recv);
					 recv = (Rssfingers)input.readObject();
					Messages.ShowMesg("Got the FINGER---Point: "+recv.pointId);
					iter = recv.addrRss.entrySet().iterator();
					while (iter.hasNext()) {
						Map.Entry<String, ArrayList<Integer>> entry = (Map.Entry<String, ArrayList<Integer>>) iter.next();
						String key = entry.getKey();
						Messages.ShowMesg(key);
						ArrayList<Integer> val = entry.getValue();
						for (Integer i: val) {
							Messages.ShowMesg(i);
						}
					}	
				}
				fingerId++; 
				fingerId = DBA.insertIntoRSSDB(a, fingerId);
				output.write(("fingerId="+fingerId).getBytes());								
				Messages.ShowMesg("fingerId="+fingerId);
				if(!(GetPointCount()<GetPointSum())){  //当指纹库中每个points收集完毕之后再执行建库过程,保证整体只建立一次
					DBA.buildRSSFingersDB();		
					Messages.ShowMesg("End the building process......");
				}				
			}
			//match process commit:adopt the NN algorithm
			else {				
				//建库过程放在定位的时候，便于加快指纹采集的过程
				
				Messages.ShowMesg("End the location process......");
				fingerId = 0;
				//This is a MatchDbAPI process.
				ArrayList<Rssfingers> a = new ArrayList<Rssfingers>();
				recv = (Rssfingers)input.readObject();				
				
				Messages.ShowMesg("Got the RSSI......");
				//dealing with fingers from clients
				while (recv.pointId != -2) {
					//put the finger into ArrayList<Rssfingers> a
					a.add(recv);
					//print it to console
					recv = (Rssfingers)input.readObject();
					Messages.ShowMesg("pointId: "+recv.pointId);
					Iterator iter = recv.addrRss.entrySet().iterator();
					while (iter.hasNext()) {
						Map.Entry<String, ArrayList<Integer>> entry = (Map.Entry<String, ArrayList<Integer>>) iter.next();
						String key = entry.getKey();
						Messages.ShowMesg(key);
						ArrayList<Integer> val = entry.getValue();
						for (Integer i: val) {
							Messages.ShowMesg(i);
						}
					}//end the print action
				}
				fingerId++; 
				fingermatch =  new FingerMatch(1);
				HashMap<String,Double> hash_macrss = fingermatch.GetNormalFingers(a);				
				String send = DBA.matchInRSSDB(hash_macrss);
				output.write((send).getBytes());		
				Messages.ShowMesg("Finish the location process,and the result as flowon......");
				Messages.ShowMesg(send);
			}        
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		finally {
			try {
				output.close();
				input.close();
				client.close();
			}
			catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}

	private int GetPointCount() {
		int curRecordedPointCount = 0;
		str_sql = "SELECT count(distinct pointid) as pointcount FROM rssfingers.rssrecord;";
		ResultSet rs;
		try {
			rs = DBHelper.executeQuery(str_sql);
			while(rs.next()){
				curRecordedPointCount = rs.getInt("pointcount");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return curRecordedPointCount;
	}

	private int GetPointSum() {
		int PointSum = 0;
		str_sql = "select count(pointid) as pointsum from points;";
		ResultSet rs;
		try {
			rs = DBHelper.executeQuery(str_sql);
			while(rs.next()){
				PointSum = rs.getInt("pointsum");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return PointSum;
	}
}
