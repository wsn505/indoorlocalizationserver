package server;

/* 
 * 2014-12-04
 * Author: Cholerae 
 * Email: choleraehyq@gmail.com
 */

//import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import common.DBHelper;
import common.Messages;

import clientNetworkAPI.Rssfingers;
import match.FingerMatch;
import normalize.normal_distribution;

public class DAO {
	
	private Connection conn;
    //Connection to DB
	private int maxPointId = 0;
	private String sql = new String();
	
    private final ReadWriteLock DbLockMap = new ReentrantReadWriteLock();
    private final Lock rMap = DbLockMap.readLock();
    private final Lock wMap = DbLockMap.writeLock();   //records add part
    private final ReadWriteLock DbLockRss = new ReentrantReadWriteLock();
    private final Lock rRss = DbLockRss.readLock();
    private final Lock wRss = DbLockRss.writeLock();
    
    FingerMatch fingermatch = new FingerMatch(1);
    
    public DAO() {
    	try {
    		Class.forName("com.mysql.jdbc.Driver");
    		Messages.ShowMesg("Driver loaded");
    		
    	}
    	catch (Exception ex) {
    		ex.printStackTrace();
    	}
    }
    
	public void LinkTheDB(String kind) { 
		try {			
			if (kind == "Map") {				
				rMap.lock();
				ResultSet rs = DBHelper.executeQuery("select max(pointid) as maxpoint from points");
				rMap.unlock();
				while(rs.next()){
					maxPointId = rs.getInt("maxpoint");
				}
			} // Get the point number in the map
			//I assume the pointId is start from 0 not 1.
			Messages.ShowMesg("Database connected");
		}
		catch (Exception ex){
			ex.printStackTrace();
		}
	}
	
	public void closeDB() throws SQLException {
		conn.close();
	}

	public synchronized int insertIntoRSSDB(ArrayList<Rssfingers> recv, int fingerId) throws SQLException {

		int curfingerid = fingerId;
		sql = "insert into rssrecord(pointid,macaddr,rss) values (?,?,?)";
		for (Rssfingers cur: recv) {
			if(cur.pointId <= maxPointId)
			{
				Set<Map.Entry<String, ArrayList<Integer>>> data = cur.addrRss.entrySet();
				for (Map.Entry<String, ArrayList<Integer>> pair : data) {
					wMap.lock(); //in fact,all DB(mysql,MSQ,ORCAL) generally have the policy of LOCK
					for (Integer mac : pair.getValue()) {						
						DBHelper.executeNonQuery(sql, cur.pointId,pair.getKey(),mac);
						curfingerid++;
					}
					wMap.unlock();
				}	
			}
		}
		//insertStatement.close();
		return curfingerid;
	}

	/*
	 * Build the final wifi fingers database
	 */
	public synchronized void buildRSSFingersDB() throws SQLException {	
		
		//清空指纹库，方便更新数据
		DBHelper.executeNonQuery("delete from rss_fingers where fingerid>0;");
				
		//lock the process building fingers map
		rMap.lock();
		wRss.lock();
		
		for (int i = 1; i <= maxPointId; i++) {

			HashMap<String, ArrayList<Double>> thisPointMacaddr = 	new HashMap<String, ArrayList<Double>>();
			ArrayList<String> macaddr = new ArrayList<String>();
			HashMap<String, Double> macrss = new HashMap<String, Double>();
			
			ResultSet rs = DBHelper.executeQuery("select macaddr,rss from rssrecord where pointId=?", String.valueOf(i));
			while (rs.next()) {
				String tmp = rs.getString("macaddr");
				Double td = (double)rs.getInt("rss");
				if (!thisPointMacaddr.containsKey(tmp)) {
					ArrayList<Double> a = new ArrayList<Double>();
					a.add(td);
					thisPointMacaddr.put(tmp, a);
					macaddr.add(tmp);
				}
				else
					thisPointMacaddr.get(tmp).add(td);
			}
			int len = macaddr.size();
			String macAddr = new String(), rss = new String();
			for (int j = 0; j < len; j++) {
				macrss.put(macaddr.get(j), 
						normal_distribution.normalize(thisPointMacaddr.get(macaddr.get(j))));
			}
			//macrss is the normalized finger need to be insert into rss_fingers DB
			if(len ==0)
				continue;
			for (int j = 0; j < len-1; j++) {
				macAddr += macaddr.get(j);
				macAddr += ";";
				rss += String.valueOf(macrss.get(macaddr.get(j)));
				rss += ";";
			}
			macAddr += macaddr.get(len-1);
			rss += String.valueOf(macrss.get(macaddr.get(len-1)));		
			//Category is waiting to be added.
			//Not completed.
			if(macaddr.size()>0||macAddr.isEmpty()){
				DBHelper.executeNonQuery("insert into rss_fingers(pointid, macaddr, rss) values (?,?,?);", i,macAddr,rss);
			}
		}// end of for-loop i;
		rMap.unlock();
		wRss.unlock();
	}
	
	//match the finger from location-required client  in database
	public String matchInRSSDB(HashMap<String,Double> hasMap_AddrRss) throws SQLException {
		//The Statements below are just for test.
		List<HashMap<String,Double>> list_hasMap_FingerInDB = fingermatch.GetFingetPrintFromDataBase();
		return fingermatch.FingersMatch(hasMap_AddrRss,list_hasMap_FingerInDB);	
	}//end the action	
}
